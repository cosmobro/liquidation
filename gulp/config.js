'use strict';

var pngquant = require('imagemin-pngquant');

module.exports = {
  root: './build',

  autoprefixerConfig: ['last 100 version', '> 1%'],

  imageminOptions: {
    progressive: true,
    use: [pngquant()],
    interlaced: true,
    multipass: true
  }
};