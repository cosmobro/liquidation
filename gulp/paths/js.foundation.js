'use strict';

module.exports = [
  './node_modules/jquery/dist/jquery.min.js',
  './node_modules/fancybox/dist/js/jquery.fancybox.js',
  './node_modules/jquery-validation/dist/jquery.validate.js',
  './node_modules/jquery-placeholder/jquery.placeholder.js',
  './node_modules/jquery.maskedinput/src/jquery.maskedinput.js'
];