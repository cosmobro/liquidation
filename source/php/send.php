<?php
/****************************
 * Orlov Artur              *
 * email: orlovarth@ya.ru   *
 * skype: orlovarth         *
 * vk: https://vk.com/oarth *
 ****************************/
error_reporting(0);
include 'config.php';
include 'lib/PHPMailer/PHPMailerAutoload.php';
if (!isset($_POST['phone'])) die("Вы не ввели свой номер телефона!");

$mail = new PHPMailer;
$mail->CharSet = 'utf-8';
$mail->setFrom($from_addr, $from_name);

if ($smtp_use) {
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Host = $smtp_host;
    $mail->SMTPAuth = true;
    $mail->Username = $smtp_user;
    $mail->Password = $smtp_pass;
    $mail->SMTPSecure = $smtp_secure;
    $mail->Port = $smtp_port;
}

// Если у формы указан получатель(ли)
if ( isset($_POST['mailto']) ) {
    foreach ($_POST['mailto'] as $key => $val) {
        $mail->AddAddress(trim($val));
    }
} else {
    foreach ($to as $k) {
        $mail->addAddress($k); // Добавление получателя
    }
}

$mail->isHTML(true);
$mail->Subject = $subject;
$mail->Body = $body;

foreach ($fields as $k => $v) {
    if (isset($_POST[$k])) {
        $mail->Body .= "<b>$v:</b> " . $_POST[$k] . "<br>";
        $mail->AltBody .= "$v: " . $_POST[$k] . "/n";
    }
}

date_default_timezone_set('Europe/Moscow');
$mail->Body .= "<b>Дата:</b> " . date("m.d.y H:i:s") . "<br>";
$mail->AltBody .= "Дата: " . date("m.d.y H:i:s") . "/n";

if(!$mail->send()) {
    // echo 'Ошибка: ' . $mail->ErrorInfo;
    header('HTTP/1.1 500 Internal Server Error');
    die();
} else {
    echo 'Заявка усппешно отправлена.';
    exit();
}
?>
