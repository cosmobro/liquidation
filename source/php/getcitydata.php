<?php
/*
     ___           ___           ___                       ___
    /  /\         /  /\         /  /\          ___        /__/\
   /  /::\       /  /::\       /  /::\        /  /\       \  \:\
  /  /:/\:\     /  /:/\:\     /  /:/\:\      /  /:/        \__\:\
 /  /:/  \:\   /  /:/~/::\   /  /:/~/:/     /  /:/     ___ /  /::\
/__/:/ \__\:\ /__/:/ /:/\:\ /__/:/ /:/___  /  /::\    /__/\  /:/\:\
\  \:\ /  /:/ \  \:\/:/__\/ \  \:\/:::::/ /__/:/\:\   \  \:\/:/__\/
 \  \:\  /:/   \  \::/       \  \::/~~~~  \__\/  \:\   \  \::/
  \  \:\/:/     \  \:\        \  \:\           \  \:\   \  \:\
   \  \::/       \  \:\        \  \:\           \__\/    \  \:\
    \__\/         \__\/         \__\/                     \__\/

                          Orlov Artur
                    email: orlovarth@ya.ru
                       skype: orlovarth
                   vk: https://vk.com/oarth
*/
include("config.php");
if (!empty($_GET['city'])) {
    $cityname = $_GET['city'];
    if (array_key_exists($cityname, $cities)) {
        echo json_encode($cities[$cityname]);
    }
}
