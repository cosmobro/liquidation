'use strict';

function createMap(addr) {
  ymaps.ready(function () {
    ymaps.geocode(addr, {
      results: 1
    }).then(function (res) {
      var firstGeoObject = res.geoObjects.get(0);
      var coordinates = firstGeoObject.geometry.getCoordinates();
      var mapCenter = [
        coordinates[0],
        coordinates[1] - 0.01
      ];
      var mapState = {
        center: mapCenter,
        zoom: 15,
        controls: ['zoomControl']
      };
      var mapOptions = {
        searchControlProvider: 'yandex#search'
      };
      var placemarkProperties = {
        hintContent: addr,
        balloonContent: addr,
        balloonContentHeader: addr
      };
      var placemarkOptions = {
        iconLayout: 'default#image',
        iconImageHref: 'assets/img/marker.png',
        iconImageSize: [75, 100],
        iconImageOffset: [-37, -100]
      };
      var myPlacemark = new ymaps.Placemark(coordinates, placemarkProperties, placemarkOptions);

      myMap = new ymaps.Map('map', mapState, mapOptions);

      // Отключения увеличения скроллом
      myMap.behaviors.disable('scrollZoom');
      myMap.geoObjects.add(myPlacemark);

      mapDragDisable();
    });
  });
}

function init() {
  window.myMap = null;
  var mapElement = $('#map');

  createMap(addr);

  // Отключение перетаскивания карты для мобильных устройств
  function mapDragDisable() {
    var minWidth = 1040;

    ymaps.ready(function () {
      if ($(window).width() < minWidth && mapElement.hasClass('draggable')) {
        myMap.behaviors.disable('drag');
        mapElement.removeClass('draggable');
      } else if (!mapElement.hasClass('draggable')) {
        myMap.behaviors.enable('drag');
        mapElement.addClass('draggable');
      }

      $(window).resize(mapDragDisable);

      // Включаем при тапе по карте
      mapElement.click(function () {
        myMap.behaviors.enable('drag');
      })
    });
  }
}

module.exports = {
  createMap: createMap,
  init: init
};