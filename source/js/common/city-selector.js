'use strict';

var mapCreate = require('./map').createMap;

function citySelector() {
  $('.js-city__link').click(function (e) {
    e.preventDefault();
    // Получение данных через ajax
    var city = $(this).data('city');
    var city_lk = $(this);
    $.ajax({
      url: 'php/getcitydata.php',
      dataType: 'json',
      data: {
        city: city,
      },
      success: function (data) {
        // console.log(data);
        // Делаем текущую ссылку города активной
        $('.js-city__link.active').toggleClass('active');
        city_lk.toggleClass('active');
        // Меняем название города в шапке
        $('.js-city__name').text(city);
        // Меняем название города в поле формы
        $('.js-city__input').val(city);
        // Меняем адрес
        $('.js-city__address').text(data.addr);
        // Меняем телефон
        var phone = data.phone.replace(/[^\d,]/g, '');
        phone = 'tel:+' + phone;
        $('.js-city__phone').html(data.phone).attr('href', phone);
        // Перерисовываем карту
        $('#map').empty();
        // Карта
        mapCreate(data.addr);
        // Закрываем окно
        $.fancybox.close();
      }
    });
    return false;
  });
}

module.exports = citySelector;