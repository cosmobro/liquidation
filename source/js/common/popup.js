'use strict';

function openPopup(event) {
  event.preventDefault();

  var element = $($(this).data('target'));
  var fancyboxOptions = {
    padding: 0,
    maxWidth: '90%',
    fitToView: false,
    helpers: {
      overlay: {
        locked: false
      }
    },
    beforeShow: function () {
      if (window.timerToCloseMsgPopup) {
        clearTimeout(timerToCloseMsgPopup);
      }
    }
  };

  $.fancybox(element, fancyboxOptions);
}

function popup() {
  $('.js-popup').click(openPopup);
}


module.exports = popup;