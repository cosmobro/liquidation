'use strict';

function scrollTo() {
  $(function () {
    var headerm = 40, // Высота меню в пикс
      excludeClass = '.show_popup'; // Исключенные классы ссылок
    $('a[href^="#"]').click(function () {
      if ($(this).is(excludeClass)) {
        return true;
      }
      var target = $(this).attr('href');
      $('html, body').animate({
        scrollTop: $(target).offset().top - headerm
      }, 800, function () {
        window.location.hash = target;
      });
      return false;
    });
  });
}

module.exports = scrollTo;