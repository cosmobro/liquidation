'use strict';

function sendForm() {
  $(function () {
    // Ввод по маске
    $('input[type="tel"]').mask("+7 999 999 99 99");
    $('input').placeholder();
    // Методы валидатора
    $.validator.addMethod('notPlaceholder', function (val, el) {
      return this.optional(el) || (val !== $(el).attr('placeholder'));
    }, $.validator.messages.required);
    $.validator.addMethod('testName', function (val, el) {
      return this.optional(el) || !/[^а-яё\.\s]|\.{2,}/ig.test(val);
    }, $.validator.messages.required);
    $.validator.addMethod('emailTest', function (val, el) {
      if (val == '' || val == $(el).attr('placeholder')) {
        return true;
      }
      return /^[a-z0-9_]+([\.\-][a-z0-9_]+)*@[a-z0-9_]+([\.\-][a-z0-9_]+)*\.[a-z]{2,6}$/i.test(val);
    }, $.validator.messages.required);
    $.validator.addMethod('phoneTest', function (val, el) {
      // console.log(val, /^\+7\s\d{3,3}\s\d{3,3}\s\d{2,2}\s\d{2,2}$/i.test(val));
      if (val == '+7 ___ ___ __ __' || val == '' || val == $(el).attr('placeholder')) {
        return true;
      }
      return /^\+7\s\d{3,3}\s\d{3,3}\s\d{2,2}\s\d{2,2}$/i.test(val);
    }, $.validator.messages.required);
    $.validator.addMethod('phoneRequired', function (val, el) {
      // console.log(val, !$(el).closest('form').hasClass('isTry2Submit'));
      return val == '+7 ___ ___ __ __' && $(el).closest('form').hasClass('isTry2Submit') ? false : true;
    }, $.validator.messages.required);

    // Валидация
    $('form').each(function () {
      $(this).validate({
        rules: {
          name: {
            testName: true,
            required: true,
            notPlaceholder: true
          },
          phone: {
            phoneTest: true,
            phoneRequired: true,
            required: true,
            notPlaceholder: true
          }
        },
        messages: {
          name: {
            required: 'ВЫ НЕ ВВЕЛИ ИМЯ',
            notPlaceholder: 'ВЫ НЕ ВВЕЛИ ИМЯ',
            testName: 'НЕКОРРЕКТНОЕ ИМЯ',

          },
          phone: {
            phoneTest: 'НЕКОРРЕКТНЫЙ НОМЕР',
            phoneRequired: 'ВЫ НЕ ВВЕЛИ ТЕЛЕФОН',
            required: 'ВЫ НЕ ВВЕЛИ ТЕЛЕФОН',
            notPlaceholder: 'ВЫ НЕ ВВЕЛИ ТЕЛЕФОН',
          }
        },
        errorClass: 'error',
        validClass: 'valid',
        errorElement: 'span',
        onkeyup: false,
        errorPlacement: function (error, element) {
          element.attr("placeholder", error[0].outerText);
        },
        success: function () {
          alert('success');
        },
        showErrors: function (errorMap, errorList) {
          var submitButton = $(this.currentForm).find('button, input:submit');
          // console.log(errorList);
          for (var error in errorList) {
            var element = $(errorList[error].element),
              errorMessage = errorList[error].message;
            // element.val('').attr('placeholder', errorMessage).addClass('__error');
            element.val(errorMessage).addClass('error').attr('placeholder', errorMessage);
            // Placeholders.enable(errorList[error].element);
            // console.log(errorMessage);
          }
        },
        submitHandler: function (form) {
          window.timerToCloseMsgPopup = null;
          $(form).toggleClass('__sending');
          // var goal = $(form).data('goal');
          $.ajax({
            type: 'POST',
            url: 'php/send.php',
            data: $(form).serialize(),
            success: function (data) {
              yaCounter35417780.reachGoal('ORDER');
              $.fancybox.close();
              $.fancybox({
                content: $('#popup-thanks').show(),
                padding: 0
              });
              timerToCloseMsgPopup = setTimeout(function () {
                $.fancybox.close();
              }, 7000);
              $(form).toggleClass('__sending');
            },
            error: function (data) {
              // console.log(data);
              $.fancybox.close();
              $.fancybox({
                content: $('#popup-error').show(),
                padding: 0
              });
              timerToCloseMsgPopup = setTimeout(function () {
                $.fancybox.close();
              }, 7000);
              $(form).toggleClass('__sending');
            }
          });
        }
      });
    });
    $('input').on('focus', function () {
      if ($(this).hasClass('error')) {
        $(this).val('').removeClass('error');
      }
    });
    $('button').click(function () {
      $(this).closest('form').addClass('isTry2Submit');
    });
  });
}

module.exports = sendForm;