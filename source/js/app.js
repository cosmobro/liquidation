'use strict';

var map = require('./common/map');
var popupInit = require('./common/popup');
var citySelector = require('./common/city-selector');
var sendForm = require('./common/send-form');
var scrollTo = require('./common/scroll-to');

$(function () {
  map.init();
  popupInit();
  citySelector();
  sendForm();
  scrollTo();
});